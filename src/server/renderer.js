import React from 'react';
import ReactDOM from 'react-dom/server';
import { Provider } from 'react-redux';
import { storeConfig } from './../middleware/storeConfig';
import App from './../appContainer';
import { ServerRoutes } from "../routes";

const renderHtml = ({path}) => {

  const initialState = {};

  const store = storeConfig(initialState);

  const renderStringOutput = ReactDOM.renderToString(
    <Provider store={store}>
      <ServerRoutes />
    </Provider>
  );

	return `
	<!doctype html>
    <html lang="en">
    <head>
	    <meta charset="utf-8">
	    <title>Node Summit App</title>
	    <link rel="stylesheet" href="main.css"/>
    </head>
   <body>
     <div id="universal-node-summit-app">${renderStringOutput}</div>
     <script type="text/javascript" src="client.bundle.js"></script>
   </body>
   </html>
	`;
}

export const html = renderHtml;
