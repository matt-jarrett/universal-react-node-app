import { combineReducers } from 'redux';
import appReducers from '../appContainer/reducers';

export default combineReducers({
  appReducers,
});