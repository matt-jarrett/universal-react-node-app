const path = require('path');

module.exports = {
  target: 'node',
  entry: [ "babel-polyfill", './server.js' ],
  mode: process.env.NODE_ENV,
  plugins: [],
  resolve: {},
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
       test: /\.css$/,
        use: [ 'css-loader/locals']
      },
      {
       test: /\.(png|svg|jpg|jpeg|gif)$/,
        use: [
             'file-loader'
           ]
       }
    ]
  },
  output: {
    filename: 'server.bundle.js',
    path: path.resolve(__dirname, 'build'),
    publicPath: '/build/',
  }
};